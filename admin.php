<?php require_once ("includes/session.php"); ?>
<?php require_once ("includes/connection.php"); ?>

<html>
    <head>
        <title>P&aacute;gina Principal</title>
        <link rel="stylesheet" type="text/css" href="main.css" />  
        <link href='images/favicon.ico' rel='shortcut icon'/>
    </head>
    <body>
        <?php 
        $login = $_SESSION['usuario_login'];
        $id = $_SESSION['usuario_id'];
        $titulo = "Seja bem vindo $login, cujo id &eacute; $id" ?>
        <?php require_once ("includes/header.php"); ?>
        <div id="bg">
        <div id="global"> 
            <?php if ($login == "administrador") {
             ?>
            <ul id="list-nav">
                <li><a href="list.php">Consultar clientes cadastrados</a></li>
                <li><a href="cadastro_admin.php">Adicionar Corretor</a></li>
                <li><a href="cadastro.php?idcorretor=<?=$id?>">Adicionar Cliente</a></li>
                <li><a href="logout.php">Logout</a></li>
                <?php } else { ?>
            <ul id="list-nav">
                <li><a href="list.php">Consultar clientes cadastrados</a></li>
                <li><a href="cadastro.php?idcorretor=<?=$id?>">Adicionar Cliente</a></li>
                <li><a href="http://www.youtube.com/embed/qhHFBcK_EdQ" target="_blank">Link para o v&iacute;deo</a></li>
                <li><a href="logout.php">Logout</a></li>
                <?php }?>
            <br />
            <br />
            <iframe src="http://www.youtube.com/embed/qhHFBcK_EdQ" width="799" height="450"></iframe>
        </div>
    </div>
        <div id="wrapper"></div>
        <?php require_once ("includes/footer.php"); ?>
    </body>
</html>
