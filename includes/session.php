<?php
	session_start();

	function logged_in() {
		return isset($_SESSION['usuario_logged']);
	}

	if (!logged_in() AND ($_SERVER['PHP_SELF']!= "/cotacao/cadastro.php")) {
		header("Location:login.php");
	} 
?>