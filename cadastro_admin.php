<?php require_once ("includes/session.php"); ?>
<?php require_once ("includes/connection.php"); ?>

<html>
  <head>
    <title>Cadastro de Corretor</title>
    <link rel="stylesheet" type="text/css" href="main.css" />  
    <link href='images/favicon.ico' rel='shortcut icon'/>
  </head>
  <body>
    <?php $titulo = "Informa&ccedil;&otilde;es necess&aacute;rias" ?>
    <?php require_once ("includes/header.php"); ?>
    <div id="bg">
    <div id="global">
        <form action="query_admin.php" name="list" method="post">
          <b>* campo obrigat&oacute;rio</b><br /><br />
          <fieldset> 

            <legend><h1>Cadastro de corretor</h1></legend>
            
            <p><label for="nome"><b>Nome*:</b></label>
              <input type="text" maxlength="30" name="usuario_login" placeholder="Digite o nome do novo corretor"/><br />
            <label for="senha"><b>Senha*:</b></label>
              <input type="password" maxlength="50" name="usuario_senha" placeholder="Digite a senha do novo corretor"/><br /> 
            <label for="email"><b>Email*:</b></label>
              <input type="text" maxlength="50" name="usuario_email" placeholder="Digite o e-mail do novo corretor"/><br />  
          </fieldset> 
          <tr><td colspan="2"><br /><input type="submit" value="Enviar"/>
          <input type="reset" value="Limpar Dados">
          <input type="button" value="Voltar" onclick="location. href= 'admin.php' ">
        </form>
      </div>
    </div>
    <?php require_once ("includes/footer.php"); ?>
  </body>
</html>

<?php
  mysql_close();
?>