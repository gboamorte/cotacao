-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: cotacao
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_plano` varchar(20) NOT NULL,
  `cliente_nome` varchar(50) NOT NULL,
  `cliente_uf` varchar(2) NOT NULL,
  `cliente_cidade` varchar(50) NOT NULL,
  `cliente_email` varchar(50) NOT NULL,
  `cliente_ddd_telres` tinyint(2) NOT NULL,
  `cliente_telres` int(20) NOT NULL,
  `cliente_ddd_telcom` int(2) NOT NULL,
  `cliente_telcom` int(20) NOT NULL,
  `cliente_ddd_telcel` tinyint(2) NOT NULL,
  `cliente_telcel` int(20) NOT NULL,
  `idade_0018` int(1) NOT NULL,
  `idade_1923` int(1) NOT NULL,
  `idade_2428` int(1) NOT NULL,
  `idade_2933` int(1) NOT NULL,
  `idade_3438` int(1) NOT NULL,
  `idade_3943` int(1) NOT NULL,
  `idade_4448` int(1) NOT NULL,
  `idade_4953` int(1) NOT NULL,
  `idade_5458` int(1) NOT NULL,
  `idade_maior_igual59` int(1) NOT NULL,
  `cliente_info` char(255) NOT NULL,
  `cliente_boletim` varchar(5) NOT NULL,
  `usuario_id` int(3) NOT NULL,
  PRIMARY KEY (`cliente_id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usuario_id` int(2) NOT NULL AUTO_INCREMENT,
  `usuario_login` varchar(30) NOT NULL,
  `usuario_senha` varchar(50) NOT NULL,
  `usuario_email` varchar(40) NOT NULL,
  `usuario_visivel` tinyint(1) NOT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-14 11:07:45
