<?php require_once ("includes/session.php"); ?>
<?php require_once ("includes/connection.php"); ?>

<html>
	<head>
		<title>Detalhes dos clientes cadastrados no banco de dados</title>
		<link rel="stylesheet" type="text/css" href="main.css" />  
		<link href='images/favicon.ico' rel='shortcut icon'/>
	</head>
	<body>
		<?php $titulo = "Confira abaixo os detalhes do cliente" ?>
		<?php require_once ("includes/header.php"); ?>
		<div id="bg">
		<div id="global">
			<?php

                    if (!array_key_exists('id', $_GET)){
                    echo '<h1 style="color:red;">Favor verificar o endere&ccedil;o do site, tem algum detalhe faltando.</h1>';

                    } 

                    $id = $_GET["id"];
                    $query = mysql_query("SELECT * FROM clientes WHERE cliente_id = '{$id}'");
                    $qlinhas = mysql_num_rows($query);

					if(!$qlinhas){
                    echo '<h1 style="color:red;">N&atilde;o existe nenhum usu&aacute;rio com o id passado no endere&ccedil;o do site.</h1>';
                    } else {
                    ?>
			<ul>
				<form>
					<fieldset>
						<legend><h1>Detalhes do cliente</h1></legend>
						<?php
							$result = mysql_query("SELECT * FROM clientes WHERE cliente_id=" . $_GET["id"], $connection);
							if (!$result) {
								die("Database query failed: " . mysql_error());
							}
							while($row = mysql_fetch_array($result)) {
								echo "ID: " . $row['cliente_id'].  "<br />";
								echo "Plano selecionado: " . $row['cliente_plano']. "<br />";
								echo "Nome: " . $row['cliente_nome'].  "<br />";
								echo "UF: " . $row['cliente_uf'].  "<br />";
								echo "Cidade: " . $row['cliente_cidade'] . "<br />";
								echo "Email: " . $row['cliente_email'].  "<br />";
								echo "(DDD) Telefone Residencial: " . "(" . $row['cliente_ddd_telres'] . ")" . " " . $row['cliente_telres']. "<br />";
								echo "(DDD) Telefone Comercial: " . "(" . $row['cliente_ddd_telcom'] . ")" . " " . $row['cliente_telcom']. "<br />";
								echo "(DDD) Telefone Celular: " . "(" . $row['cliente_ddd_telcel'] . ")" . " " . $row['cliente_telcel']. "<br />";
								echo "Pessoas com idade de 0 at&eacute; 18: " . $row['idade_0018']. "<br />";
								echo "Pessoas com idade de 19 at&eacute; 23: " . $row['idade_1923']. "<br />";
								echo "Pessoas com idade de 24 at&eacute; 28: " . $row['idade_2428']. "<br />";
								echo "Pessoas com idade de 29 at&eacute; 33: " . $row['idade_2933']. "<br />";
								echo "Pessoas com idade de 34 at&eacute; 38: " . $row['idade_3438']. "<br />";
								echo "Pessoas com idade de 39 at&eacute; 43: " . $row['idade_3943']. "<br />";
								echo "Pessoas com idade de 44 at&eacute; 48: " . $row['idade_4448']. "<br />";
								echo "Pessoas com idade de 49 at&eacute; 53: " . $row['idade_4953']. "<br />";
								echo "Pessoas com idade de 54 at&eacute; 58: " . $row['idade_5458']. "<br />";
								echo "Pessoas com idade maior ou igual de 59: " . $row['idade_maior_igual59']. "<br />";
								echo "Informa&ccedil;&otilde;es adicionais: " . $row['cliente_info'] . "<br />";
								echo "Gostaria de receber nosso boletim informativo: " . $row['cliente_boletim'].  "<br />";
							}
							mysql_close();
						?>
					</fieldset>
				</form>
				<input type="button" value="P&aacute;gina principal" onclick="location.href = 'admin.php'">
			</ul>

			<?php }?>
		</div>
	</div>
		<?php require_once ("includes/footer.php"); ?>
	</body>
</html>