<?php require_once ("includes/connection.php"); ?>

<html>
  <head>
    <title>Confirma&ccedil;&atilde;o de dados</title>
    <link rel="stylesheet" type="text/css" href="main.css" />  
    <link href='images/favicon.ico' rel='shortcut icon'/>
  </head>
  <body>
    <?php $titulo = "Confirma&ccedil;&atilde;o de dados de cliente" ?>
    <?php require_once ("includes/header.php"); ?>
    <div id="bg">
    <div id="global">
      <form action="obrigado.php" name="list" method="post">
      <input type="hidden" value="<?=$_POST['cliente_plano']?>" name="cliente_plano">
      <input type="hidden" value="<?=$_POST['cliente_nome']?>" name="cliente_nome">
      <input type="hidden" value="<?=$_POST['cliente_uf']?>" name="cliente_uf">
      <input type="hidden" value="<?=$_POST['cliente_cidade']?>" name="cliente_cidade">
      <input type="hidden" value="<?=$_POST['cliente_email']?>" name="cliente_email">
      <input type="hidden" value="<?=$_POST['cliente_ddd_telres']?>" name="cliente_ddd_telres">
      <input type="hidden" value="<?=$_POST['cliente_telres']?>" name="cliente_telres">
      <input type="hidden" value="<?=$_POST['cliente_ddd_telcom']?>" name="cliente_ddd_telcom">
      <input type="hidden" value="<?=$_POST['cliente_telcom']?>" name="cliente_telcom">
      <input type="hidden" value="<?=$_POST['cliente_ddd_telcel']?>" name="cliente_ddd_telcel">
      <input type="hidden" value="<?=$_POST['cliente_telcel']?>" name="cliente_telcel">
      <input type="hidden" value="<?=$_POST['idade_1']?>" name="idade_1">
      <input type="hidden" value="<?=$_POST['idade_2']?>" name="idade_2">
      <input type="hidden" value="<?=$_POST['idade_3']?>" name="idade_3">
      <input type="hidden" value="<?=$_POST['idade_4']?>" name="idade_4">
      <input type="hidden" value="<?=$_POST['idade_5']?>" name="idade_5">
      <input type="hidden" value="<?=$_POST['idade_6']?>" name="idade_6">
      <input type="hidden" value="<?=$_POST['idade_7']?>" name="idade_7">
      <input type="hidden" value="<?=$_POST['idade_8']?>" name="idade_8">
      <input type="hidden" value="<?=$_POST['idade_9']?>" name="idade_9">
      <input type="hidden" value="<?=$_POST['idade_10']?>" name="idade_10">
      <input type="hidden" value="<?=$_POST['cliente_info']?>" name="cliente_info">
      <input type="hidden" value="<?=$_POST['cliente_boletim']?>" name="cliente_boletim">
      <input type="hidden" value="<?=$_POST['idcorretor']?>" name="idcorretor">
      <?php
        $plano = $_POST['cliente_plano'];
        $nome = $_POST['cliente_nome'];
        $uf = $_POST['cliente_uf'];
        $cidade = $_POST['cliente_cidade'];
        $email = $_POST['cliente_email'];
        $ddd_tel_res = $_POST['cliente_ddd_telres'];
        $tel_res = $_POST['cliente_telres'];
        $ddd_tel_com = $_POST['cliente_ddd_telcom'];
        $tel_com = $_POST['cliente_telcom'];
        $ddd_tel_cel = $_POST['cliente_ddd_telcel'];
        $tel_cel = $_POST['cliente_telcel'];
        $idade_1 = $_POST['idade_1'];
        $idade_2 = $_POST['idade_2'];
        $idade_3 = $_POST['idade_3'];
        $idade_4 = $_POST['idade_4'];
        $idade_5 = $_POST['idade_5'];
        $idade_6 = $_POST['idade_6'];
        $idade_7 = $_POST['idade_7'];
        $idade_8 = $_POST['idade_8'];
        $idade_9 = $_POST['idade_9'];
        $idade_10 = $_POST['idade_10'];
        $info = $_POST['cliente_info'];
        $boletim = $_POST['cliente_boletim'];
        $usuarioid = $_POST['idcorretor'];

        $query = mysql_query("SELECT * FROM clientes WHERE cliente_email = '{$email}'");
        $qlinhas = mysql_num_rows($query);
        $erro = true;

          for ($count=1; $count <=10 ; $count++) { 
          if ($_POST['idade_' . $count] >0) {
            $erro = false;
            break;
          }
      }

        if (empty($plano) or 
          (empty($nome)) or 
          (empty($cidade)) or 
          (empty($email)) or
          (empty($tel_res)) or 
          (empty($tel_cel))){
          echo '<h1 style="color:red;">Oops, retorne ao formul&aacute;rio e verifique os campos obrigat&oacuterios.</h1>';
        } elseif ($qlinhas) {
          echo '<h1 style="color:red;">Oops, j&aacute existe um cliente com este e-mail.</h1>';
        } elseif ($erro) {
          echo '<h1 style="color:red;">Favor, verificar o campo de idade, o mesmo est&aacute; vazio ou cont&eacute;m letras.</h1>';
        } elseif (!is_numeric($tel_res)) {
          echo '<h1 style="color:red;">Favor, verificar os campo relativo ao telefone residencial.</h1>';
        } elseif (!is_numeric($tel_cel)) {
          echo '<h1 style="color:red;">Favor, verificar os campo relativo ao telefone celular.</h1>';
        } else
        {
        ?>
          <?php echo "Confirme seus dados"; ?><br />
          <?php echo "Plano de sa&uacute;de selecionado: " . $_POST['cliente_plano']; ?><br />
          <?php echo "Nome: " . $_POST['cliente_nome']; ?><br />
          <?php echo "UF: " . $_POST['cliente_uf']; ?><br />
          <?php echo "Cidade: " . $_POST['cliente_cidade']; ?><br />
          <?php echo "Email: " . $_POST['cliente_email']; ?><br />
          <?php echo "(DDD) Telefone Residencial: " . $_POST['cliente_ddd_telres'] . " " . $_POST['cliente_telres']; ?><br />
          <?php echo "(DDD) Telefone Comercial: " . $_POST['cliente_ddd_telcom'] . " " . $_POST['cliente_telcom']; ?><br />
          <?php echo "(DDD) Telefone Celular: " . $_POST['cliente_ddd_telcel'] . " " . $_POST['cliente_telcel']; ?><br />
          <?php echo "Pessoas com idade de 0 at&eacute; 18: " . $_POST['idade_1']; ?><br />
          <?php echo "Pessoas com idade de 19 at&eacute; 23: " . $_POST['idade_2']; ?><br />
          <?php echo "Pessoas com idade de 24 at&eacute; 28: " . $_POST['idade_3']; ?><br />
          <?php echo "Pessoas com idade de 29 at&eacute; 33: " . $_POST['idade_4']; ?><br />
          <?php echo "Pessoas com idade de 34 at&eacute; 38: " . $_POST['idade_5']; ?><br />
          <?php echo "Pessoas com idade de 39 at&eacute; 43: " . $_POST['idade_6']; ?><br />
          <?php echo "Pessoas com idade de 44 at&eacute; 48: " . $_POST['idade_7']; ?><br />
          <?php echo "Pessoas com idade de 49 at&eacute; 53: " . $_POST['idade_8']; ?><br />
          <?php echo "Pessoas com idade de 54 at&eacute; 58: " . $_POST['idade_9']; ?><br />
          <?php echo "Pessoas com idade igual ou maior de 59: " . $_POST['idade_10']; ?><br />
          <?php echo "Informa&ccedil;&otilde;es adicionais: " . $_POST['cliente_info']; ?><br />
          <?php echo "Gostaria de receber nosso boletim informativo: " . $_POST['cliente_boletim']; ?><br />
          <br />
          <tr><td colspan="2"><br /><input type="submit" value="Valores corretos" />
          <input type="button" value="Voltar" onclick="history.go(-1)">
      <?php }?>
    </div>
  </div>
    <?php require_once ("includes/footer.php"); ?>
  </body>
</html>