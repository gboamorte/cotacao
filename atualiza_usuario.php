<?php require_once ("includes/connection.php"); ?>
<?php require_once ("includes/session.php"); ?>

<html>
    <head>
        <title>Atualiza&ccedil;&atilde;o de clientes</title>
        <link rel="stylesheet" type="text/css" href="main.css" />  
        <link href='images/favicon.ico' rel='shortcut icon'/>
    </head>
    <body>
        <?php $titulo = "Atualizar informa&ccedil;&otilde;es do cliente" ?>
        <?php require_once ("includes/header.php"); ?>
        <div id="bg">

            <div id="global">
                <?php

                    if (!array_key_exists('id', $_GET)){
                    echo '<h1 style="color:red;">Favor verificar o endere&ccedil;o do site, tem algum detalhe faltando.</h1>';

                    } 

                    $id = $_GET["id"];
                    $query = mysql_query("SELECT * FROM clientes WHERE cliente_id = '{$id}'");
                    $qlinhas = mysql_num_rows($query);

                    if(!$qlinhas){
                    echo '<h1 style="color:red;">N&atilde;o existe nenhum usu&aacute;rio com o id passado no endere&ccedil;o do site.</h1>';
                    } else {
                    ?>
                <?php
                            $result = mysql_query("SELECT * FROM clientes WHERE cliente_id=" . $_GET["id"], $connection);
                            if (!$result) {
                                die("Database query failed: " . mysql_error());
                            }
                            while($row = mysql_fetch_array($result)) {
                                $plano = $row['cliente_plano'];
                                $nome = $row['cliente_nome'];
                                $cidade = $row['cliente_cidade'];
                                $email = $row['cliente_email'];
                                $dddtelres = $row['cliente_ddd_telres'];
                                $telres = $row['cliente_telres'];
                                $dddtelcom = $row['cliente_ddd_telcom'];
                                $telcom = $row['cliente_telcom'];
                                $dddtelcel = $row['cliente_ddd_telcel'];
                                $telcel = $row['cliente_telcel'];
                                $idade_0018 = $row['idade_0018'];
                                $idade_1923 = $row['idade_1923'];
                                $idade_2428 = $row['idade_2428'];
                                $idade_2933 = $row['idade_2933'];
                                $idade_3438 = $row['idade_3438'];
                                $idade_3943 = $row['idade_3943'];
                                $idade_4448 = $row['idade_4448'];
                                $idade_4953 = $row['idade_4953'];
                                $idade_5458 = $row['idade_5458'];
                                $maior59 = $row['idade_maior_igual59'];
                                $info = $row['cliente_info'];
                                $boletim = $row['cliente_boletim'];
                            }
                            mysql_close();
                        ?>

                     <form action="atualiza_query.php" method="post">

                        <div id="conteúdo">

                            <b>* campo obrigat&oacute;rio</b><br /><br />
                            <fieldset> 
                                <legend><h1>Informa&ccedil;&otilde;es b&aacute;sicas</h1></legend>
                                    <input type="hidden" name="idcorretor" value="<?=$_GET['id']?>">
                                <p><b>Selecione o plano*:</b> <select name="cliente_plano" >
                                <optgroup label="Plano selecionado pelo usu&aacute;rio">
                                    <option><?php echo "$plano" ?></option>
                                </optgroup>
                                <optgroup label="Planos dispon&iacute;veis">
                                    <option value="Individual">Individual</option>
                                    <option value="Familiar">Familiar</option>
                                    <option value="Empresarial">Empresarial</option>
                                </optgroup>
                                </select>

                                <p><label for="nome"><b>Nome*:</b></label>
                                    <input type="text" maxlength="50" name="cliente_nome" value="<?php echo "$nome" ?>" placeholder="Digite seu nome"/> 
                                UF: <select class="select1" name="cliente_uf" id="cliente_uf" onchange="cid_est(this.value)" onkeypress="return mask_cpf(this,90,event)" style="padding: 1px 2px;">
                                <option>SP</option>

                                </select> <b>Cidade*:</b> <select name="cliente_cidade" class="select1" id="cliente_cidade" style="width: 160px; padding: 1px 2px;" onkeypress="return mask_cpf(this,90,event)" >
                                <optgroup label="Cidade selecionada pelo usu&aacute;rio">
                                    <option><?php echo "$cidade" ?></option>
                                </optgroup>
                                <optgroup label="Cidades dispon&iacute;veis">
                                    <option>Adamantina</option>
                                    <option>Adolfo</option>
                                    <option>Agisse</option>
                                    <option>Agua Vermelha</option>
                                    <option>Aguai</option>
                                    <option>Aguas da Prata</option>
                                    <option>Aguas de Lindoia</option>
                                    <option>Aguas de Santa Barbara</option>
                                    <option>Aguas de Sao Pedro</option>
                                    <option>Agudos</option>
                                    <option>Agulha</option>
                                    <option>Ajapi</option>
                                    <option>Alambari</option>
                                    <option>Alberto Moreira</option>
                                    <option>Aldeia</option>
                                    <option>Aldeia de Carapicuiba</option>
                                    <option>Alfredo Guedes</option>
                                    <option>Alfredo Marcondes</option>
                                    <option>Altair</option>
                                    <option>Altinopolis</option>
                                    <option>Alto Alegre</option>
                                    <option>Alto Pora</option>
                                    <option>Aluminio</option>
                                    <option>Alvares Florence</option>
                                    <option>Alvares Machado</option>
                                    <option>Alvaro de Carvalho</option>
                                    <option >Alvinlandia</option>
                                    <option>Amadeu Amaral</option>
                                    <option>Amandaba</option>
                                    <option>Ameliopolis</option>
                                    <option>Americana</option>
                                    <option>Americo Brasiliense</option>
                                    <option>Americo de Campos</option>
                                    <option>Amparo</option>
                                    <option>Ana Dias</option>
                                    <option>Analandia</option>
                                    <option>Anapolis</option>
                                    <option>Andes</option>
                                    <option>Andradina</option>
                                    <option>Angatuba</option>
                                    <option>Anhembi</option>
                                    <option>Anhumas</option>
                                    <option>Aparecida</option>
                                    <option>Aparecida D'oeste</option>
                                    <option>Aparecida de Monte Alto</option>
                                    <option>Aparecida de Sao Manuel</option>
                                    <option>Aparecida do Bonito</option>
                                    <option>Apiai</option>
                                    <option>Apiai-mirim</option>
                                    <option>Arabela</option>
                                    <option>Aracacu</option>
                                    <option>Aracaiba</option>
                                    <option>Aracariguama</option>
                                    <option>Aracatuba</option>
                                    <option>Aracoiaba da Serra</option>
                                    <option>Aramina</option>
                                    <option>Arandu</option>
                                    <option>Arapei</option>
                                    <option>Araraquara</option>
                                    <option>Araras</option>
                                    <option>Araxas</option>
                                    <option>Arcadas</option>
                                    <option>Arco-iris</option>
                                    <option>Arealva</option>
                                    <option>Areias</option>
                                    <option>Areiopolis</option>
                                    <option>Ariranha</option>
                                    <option>Ariri</option>
                                    <option>Artemis</option>
                                    <option>Artur Nogueira</option>
                                    <option>Aruja</option>
                                    <option>Aspasia</option>
                                    <option>Assis</option>
                                    <option>Assistencia</option>
                                    <option>Atibaia</option>
                                    <option>Atlantida</option>
                                    <option>Auriflama</option>
                                    <option>Avai</option>
                                    <option>Avanhandava</option>
                                    <option>Avare</option>
                                    <option>Avencas</option>
                                    <option>Bacaetava</option>
                                    <option>Bacuriti</option>
                                    <option>Bady Bassitt</option>
                                    <option>Baguacu</option>
                                    <option>Bairro Alto</option>
                                    <option>Balbinos</option>
                                    <option>Balsamo</option>
                                    <option>Bananal</option>
                                    <option>Bandeirantes D'oeste</option>
                                    <option>Barao Ataliba Nogueira</option>
                                    <option>Barao de Antonina</option>
                                    <option>Barao de Geraldo</option>
                                    <option>Barbosa</option>
                                    <option>Bariri</option>
                                    <option>Barra Bonita</option>
                                    <option>Barra do Chapeu</option>
                                    <option>Barra do Turvo</option>
                                    <option>Barra Dourada</option>
                                    <option>Barrania</option>
                                    <option>Barretos</option>
                                    <option>Barrinha</option>
                                    <option>Barueri</option>
                                    <option>Bastos</option>
                                    <option>Batatais</option>
                                    <option>Batatuba</option>
                                    <option>Batista Botelho</option>
                                    <option>Bauru</option>
                                    <option>Bebedouro</option>
                                    <option>Bela Floresta</option>
                                    <option>Bela Vista Sao-carlense</option>
                                    <option>Bento de Abreu</option>
                                    <option>Bernardino de Campos</option>
                                    <option>Bertioga</option>
                                    <option>Bilac</option>
                                    <option>Birigui</option>
                                    <option>Biritiba-mirim</option>
                                    <option>Biritiba-ussu</option>
                                    <option>Boa Esperanca do Sul</option>
                                    <option>Boa Vista dos Andradas</option>
                                    <option>Boa Vista Paulista</option>
                                    <option>Bocaina</option>
                                    <option>Bofete</option>
                                    <option>Boituva</option>
                                    <option>Bom Fim do Bom Jesus</option>
                                    <option>Bom Jesus dos Perdoes</option>
                                    <option>Bom Retiro da Esperanca</option>
                                    <option>Bom Sucesso de Itarare</option>
                                    <option>Bonfim Paulista</option>
                                    <option>Bora</option>
                                    <option>Boraceia</option>
                                    <option>Borborema</option>
                                    <option>Borebi</option>
                                    <option>Botafogo</option>
                                    <option>Botelho</option>
                                    <option>Botucatu</option>
                                    <option>Botujuru</option>
                                    <option>Braco</option>
                                    <option>Braganca Paulista</option>
                                    <option>Bras Cubas</option>
                                    <option>Brasitania</option>
                                    <option>Brauna</option>
                                    <option>Brejo Alegre</option>
                                    <option>Brodowski</option>
                                    <option>Brotas</option>
                                    <option>Bueno de Andrada</option>
                                    <option>Buri</option>
                                    <option>Buritama</option>
                                    <option>Buritizal</option>
                                    <option>Cabralia Paulista</option>
                                    <option>Cabreuva</option>
                                    <option>Cacapava</option>
                                    <option>Cachoeira de Emas</option>
                                    <option>Cachoeira Paulista</option>
                                    <option>Caconde</option>
                                    <option>Cafelandia</option>
                                    <option>Cafesopolis</option>
                                    <option>Caiabu</option>
                                    <option>Caibura</option>
                                    <option>Caieiras</option>
                                    <option>Caiua</option>
                                    <option>Cajamar</option>
                                    <option>Cajati</option>
                                    <option>Cajobi</option>
                                    <option>Cajuru</option>
                                    <option>Cambaquara</option>
                                    <option>Cambaratiba</option>
                                    <option>Campestrinho</option>
                                    <option>Campina de Fora</option>
                                    <option>Campina do Monte Alegre</option>
                                    <option>Campinal</option>
                                    <option>Campinas</option>
                                    <option>Campo Limpo Paulista</option>
                                    <option>Campos de Cunha</option>
                                    <option>Campos do Jordao</option>
                                    <option>Campos Novos Paulista</option>
                                    <option>Cananeia</option>
                                    <option>Canas</option>
                                    <option>Candia</option>
                                    <option>Candido Mota</option>
                                    <option>Candido Rodrigues</option>
                                    <option>Canguera</option>
                                    <option>Canitar</option>
                                    <option>Capao Bonito</option>
                                    <option>Capela do Alto</option>
                                    <option>Capivari</option>
                                    <option>Capivari da Mata</option>
                                    <option>Caporanga</option>
                                    <option>Capuava</option>
                                    <option>Caraguatatuba</option>
                                    <option>Carapicuiba</option>
                                    <option>Cardeal</option>
                                    <option>Cardoso</option>
                                    <option>Caruara</option>
                                    <option>Casa Branca</option>
                                    <option>Cassia dos Coqueiros</option>
                                    <option>Castilho</option>
                                    <option>Catanduva</option>
                                    <option>Catigua</option>
                                    <option>Catucaba</option>
                                    <option>Caucaia do Alto</option>
                                    <option>Cedral</option>
                                    <option>Cerqueira Cesar</option>
                                    <option>Cerquilho</option>
                                    <option>Cesario Lange</option>
                                    <option>Cezar de Souza</option>
                                    <option>Charqueada</option>
                                    <option>Chavantes</option>
                                    <option>Cipo-guacu</option>
                                    <option>Clarinia</option>
                                    <option>Clementina</option>
                                    <option>Cocaes</option>
                                    <option>Colina</option>
                                    <option>Colombia</option>
                                    <option>Conceicao de Monte Alegre</option>
                                    <option>Conchal</option>
                                    <option>Conchas</option>
                                    <option>Cordeiropolis</option>
                                    <option>Coroados</option>
                                    <option>Coronel Goulart</option>
                                    <option>Coronel Macedo</option>
                                    <option>Corredeira</option>
                                    <option>Corrego Rico</option>
                                    <option>Corumbatai</option>
                                    <option>Cosmopolis</option>
                                    <option>Cosmorama</option>
                                    <option>Costa Machado</option>
                                    <option>Cotia</option>
                                    <option>Cravinhos</option>
                                    <option>Cristais Paulista</option>
                                    <option>Cruz das Posses</option>
                                    <option>Cruzalia</option>
                                    <option>Cruzeiro</option>
                                    <option>Cubatao</option>
                                    <option>Cuiaba Paulista</option>
                                    <option>Cunha</option>
                                    <option>Curupa</option>
                                    <option>Dalas</option>
                                    <option>Descalvado</option>
                                    <option>Diadema</option>
                                    <option>Dirce Reis</option>
                                    <option>Dirceu</option>
                                    <option>Divinolandia</option>
                                    <option>Dobrada</option>
                                    <option>Dois Corregos</option>
                                    <option>Dolcinopolis</option>
                                    <option>Domelia</option>
                                    <option>Dourado</option>
                                    <option>Dracena</option>
                                    <option>Duartina</option>
                                    <option>Dumont</option>
                                    <option>Duplo Ceu</option>
                                    <option>Echapora</option>
                                    <option>Eldorado</option>
                                    <option>Eleuterio</option>
                                    <option>Elias Fausto</option>
                                    <option>Elisiario</option>
                                    <option>Embauba</option>
                                    <option>Embu</option>
                                    <option>Embu-guacu</option>
                                    <option>Emilianopolis</option>
                                    <option>Eneida</option>
                                    <option>Engenheiro Balduino</option>
                                    <option>Engenheiro Coelho</option>
                                    <option>Engenheiro Maia</option>
                                    <option>Engenheiro Schmidt</option>
                                    <option>Esmeralda</option>
                                    <option>Esperanca D'oeste</option>
                                    <option>Espigao</option>
                                    <option>Espirito Santo do Pinhal</option>
                                    <option>Espirito Santo do Turvo</option>
                                    <option>Estiva Gerbi</option>
                                    <option>Estrela D'oeste</option>
                                    <option>Estrela do Norte</option>
                                    <option>Euclides da Cunha Paulista</option>
                                    <option>Eugenio de Melo</option>
                                    <option>Fartura</option>
                                    <option>Fatima</option>
                                    <option>Fatima Paulista</option>
                                    <option>Fazenda Velha</option>
                                    <option>Fernando Prestes</option>
                                    <option>Fernandopolis</option>
                                    <option>Fernao</option>
                                    <option>Ferraz de Vasconcelos</option>
                                    <option>Flora Rica</option>
                                    <option>Floreal</option>
                                    <option>Floresta do Sul</option>
                                    <option>Florida Paulista</option>
                                    <option>Florinea</option>
                                    <option>Franca</option>
                                    <option>Francisco Morato</option>
                                    <option>Franco da Rocha</option>
                                    <option>Frutal do Campo</option>
                                    <option>Gabriel Monteiro</option>
                                    <option>Galia</option>
                                    <option>Garca</option>
                                    <option>Gardenia</option>
                                    <option>Gastao Vidigal</option>
                                    <option>Gaviao Peixoto</option>
                                    <option>General Salgado</option>
                                    <option>Getulina</option>
                                    <option>Glicerio</option>
                                    <option>Gramadinho</option>
                                    <option>Guachos</option>
                                    <option>Guaianas</option>
                                    <option>Guaicara</option>
                                    <option>Guaimbe</option>
                                    <option>Guaira</option>
                                    <option>Guamium</option>
                                    <option>Guapiacu</option>
                                    <option>Guapiara</option>
                                    <option>Guapiranga</option>
                                    <option>Guara</option>
                                    <option>Guaracai</option>
                                    <option>Guaraci</option>
                                    <option>Guaraciaba D'oeste</option>
                                    <option>Guarani D'oeste</option>
                                    <option>Guaranta</option>
                                    <option>Guarapiranga</option>
                                    <option>Guarapua</option>
                                    <option>Guararapes</option>
                                    <option>Guararema</option>
                                    <option>Guaratingueta</option>
                                    <option>Guarei</option>
                                    <option>Guariba</option>
                                    <option>Guariroba</option>
                                    <option>Guarizinho</option>
                                    <option>Guaruja</option>
                                    <option>Guarulhos</option>
                                    <option>Guatapara</option>
                                    <option>Guzolandia</option>
                                    <option>Herculandia</option>
                                    <option>Holambra</option>
                                    <option>Holambra Ii</option>
                                    <option>Hortolandia</option>
                                    <option>Iacanga</option>
                                    <option>Iacri</option>
                                    <option>Iaras</option>
                                    <option>Ibate</option>
                                    <option>Ibiporanga</option>
                                    <option>Ibira</option>
                                    <option>Ibirarema</option>
                                    <option>Ibitinga</option>
                                    <option>Ibitiruna</option>
                                    <option>Ibitiuva</option>
                                    <option>Ibitu</option>
                                    <option>Ibiuna</option>
                                    <option>Icem</option>
                                    <option>Ida Iolanda</option>
                                    <option>Iepe</option>
                                    <option>Igacaba</option>
                                    <option>Igaracu do Tiete</option>
                                    <option>Igarai</option>
                                    <option>Igarapava</option>
                                    <option>Igarata</option>
                                    <option>Iguape</option>
                                    <option>Ilha Comprida</option>
                                    <option>Ilha Diana</option>
                                    <option>Ilha Solteira</option>
                                    <option>Ilhabela</option>
                                    <option>Indaia do Aguapei</option>
                                    <option>Indaiatuba</option>
                                    <option>Indiana</option>
                                    <option>Indiapora</option>
                                    <option>Ingas</option>
                                    <option>Inubia Paulista</option>
                                    <option>Ipaussu</option>
                                    <option>Ipero</option>
                                    <option>Ipeuna</option>
                                    <option>Ipigua</option>
                                    <option>Iporanga</option>
                                    <option>Ipua</option>
                                    <option>Iracemapolis</option>
                                    <option>Irape</option>
                                    <option>Irapua</option>
                                    <option>Irapuru</option>
                                    <option>Itabera</option>
                                    <option>Itaboa</option>
                                    <option>Itai</option>
                                    <option>Itaiuba</option>
                                    <option>Itajobi</option>
                                    <option>Itaju</option>
                                    <option>Itanhaem</option>
                                    <option>Itaoca</option>
                                    <option>Itapecerica da Serra</option>
                                    <option>Itapetininga</option>
                                    <option>Itapeuna</option>
                                    <option>Itapeva</option>
                                    <option >Itapevi</option>
                                    <option>Itapira</option>
                                    <option>Itapirapua Paulista</option>
                                    <option>Itapolis</option>
                                    <option>Itaporanga</option>
                                    <option>Itapui</option>
                                    <option>Itapura</option>
                                    <option>Itaquaquecetuba</option>
                                    <option>Itaqueri da Serra</option>
                                    <option>Itarare</option>
                                    <option>Itariri</option>
                                    <option>Itatiba</option>
                                    <option>Itatinga</option>
                                    <option>Itirapina</option>
                                    <option>Itirapua</option>
                                    <option>Itobi</option>
                                    <option>Itororo do Paranapanema</option>
                                    <option>Itu</option>
                                    <option>Itupeva</option>
                                    <option>Ituverava</option>
                                    <option>Iubatinga</option>
                                    <option>Jaborandi</option>
                                    <option>Jaboticabal</option>
                                    <option>Jacare</option>
                                    <option>Jacarei</option>
                                    <option>Jaci</option>
                                    <option>Jacipora</option>
                                    <option>Jacuba</option>
                                    <option>Jacupiranga</option>
                                    <option>Jafa</option>
                                    <option>Jaguariuna</option>
                                    <option>Jales</option>
                                    <option>Jamaica</option>
                                    <option>Jambeiro</option>
                                    <option>Jandira</option>
                                    <option>Jardim Belval</option>
                                    <option>Jardim Presidente Dutra</option>
                                    <option>Jardim Santa Luzia</option>
                                    <option>Jardim Silveira</option>
                                    <option>Jardinopolis</option>
                                    <option>Jarinu</option>
                                    <option>Jatoba</option>
                                    <option>Jau</option>
                                    <option>Jeriquara</option>
                                    <option>Joanopolis</option>
                                    <option>Joao Ramalho</option>
                                    <option>Joaquim Egidio</option>
                                    <option>Jordanesia</option>
                                    <option>Jose Bonifacio</option>
                                    <option>Juliania</option>
                                    <option>Julio Mesquita</option>
                                    <option>Jumirim</option>
                                    <option>Jundiai</option>
                                    <option>Jundiapeba</option>
                                    <option>Junqueira</option>
                                    <option>Junqueiropolis</option>
                                    <option>Juquia</option>
                                    <option>Juquiratiba</option>
                                    <option>Juquitiba</option>
                                    <option>Juritis</option>
                                    <option>Juruce</option>
                                    <option>Jurupeba</option>
                                    <option>Jurupema</option>
                                    <option>Lacio</option>
                                    <option>Lagoa Azul</option>
                                    <option>Lagoa Branca</option>
                                    <option>Lagoinha</option>
                                    <option>Laranjal Paulista</option>
                                    <option>Laras</option>
                                    <option>Lauro Penteado</option>
                                    <option>Lavinia</option>
                                    <option>Lavrinhas</option>
                                    <option>Leme</option>
                                    <option>Lencois Paulista</option>
                                    <option>Limeira</option>
                                    <option>Lindoia</option>
                                    <option>Lins</option>
                                    <option>Lobo</option>
                                    <option>Lorena</option>
                                    <option>Lourdes</option>
                                    <option>Louveira</option>
                                    <option>Lucelia</option>
                                    <option>Lucianopolis</option>
                                    <option>Luis Antonio</option>
                                    <option>Luiziania</option>
                                    <option>Lupercio</option>
                                    <option>Lusitania</option>
                                    <option>Lutecia</option>
                                    <option>Macatuba</option>
                                    <option>Macaubal</option>
                                    <option>Macedonia</option>
                                    <option>Macucos</option>
                                    <option>Magda</option>
                                    <option>Mailasqui</option>
                                    <option>Mairinque</option>
                                    <option>Mairipora</option>
                                    <option>Major Prado</option>
                                    <option>Manduri</option>
                                    <option>Mangaratu</option>
                                    <option>Maraba Paulista</option>
                                    <option>Maracai</option>
                                    <option>Marapoama</option>
                                    <option>Marcondesia</option>
                                    <option>Maresias</option>
                                    <option>Mariapolis</option>
                                    <option>Marilia</option>
                                    <option>Marinopolis</option>
                                    <option>Maristela</option>
                                    <option>Martim Francisco</option>
                                    <option>Martinho Prado Junior</option>
                                    <option>Martinopolis</option>
                                    <option>Matao</option>
                                    <option>Maua</option>
                                    <option>Mendonca</option>
                                    <option>Meridiano</option>
                                    <option>Mesopolis</option>
                                    <option>Miguelopolis</option>
                                    <option>Mineiros do Tiete</option>
                                    <option>Mira Estrela</option>
                                    <option>Miracatu</option>
                                    <option>Miraluz</option>
                                    <option>Mirandopolis</option>
                                    <option>Mirante do Paranapanema</option>
                                    <option>Mirassol</option>
                                    <option>Mirassolandia</option>
                                    <option>Mococa</option>
                                    <option>Mogi das Cruzes</option>
                                    <option>Mogi-guacu</option>
                                    <option>Mogi-mirim</option>
                                    <option>Mombuca</option>
                                    <option>Moncoes</option>
                                    <option>Mongagua</option>
                                    <option>Montalvao</option>
                                    <option>Monte Alegre do Sul</option>
                                    <option>Monte Alto</option>
                                    <option>Monte Aprazivel</option>
                                    <option>Monte Azul Paulista</option>
                                    <option>Monte Cabrao</option>
                                    <option>Monte Castelo</option>
                                    <option>Monte Mor</option>
                                    <option>Monte Verde Paulista</option>
                                    <option>Monteiro Lobato</option>
                                    <option>Moreira Cesar</option>
                                    <option>Morro Agudo</option>
                                    <option>Morro do Alto</option>
                                    <option>Morungaba</option>
                                    <option>Mostardas</option>
                                    <option>Motuca</option>
                                    <option>Mourao</option>
                                    <option>Murutinga do Sul</option>
                                    <option>Nantes</option>
                                    <option>Narandiba</option>
                                    <option>Natividade da Serra</option>
                                    <option>Nazare Paulista</option>
                                    <option>Neves Paulista</option>
                                    <option>Nhandeara</option>
                                    <option>Nipoa</option>
                                    <option>Nogueira</option>
                                    <option>Nossa Senhora do Remedio</option>
                                    <option>Nova Alexandria</option>
                                    <option>Nova Alianca</option>
                                    <option>Nova America</option>
                                    <option>Nova Aparecida</option>
                                    <option>Nova Campina</option>
                                    <option>Nova Canaa Paulista</option>
                                    <option>Nova Castilho</option>
                                    <option>Nova Europa</option>
                                    <option>Nova Granada</option>
                                    <option>Nova Guataporanga</option>
                                    <option>Nova Independencia</option>
                                    <option>Nova Itapirema</option>
                                    <option>Nova Luzitania</option>
                                    <option>Nova Odessa</option>
                                    <option>Nova Patria</option>
                                    <option>Nova Veneza</option>
                                    <option>Novais</option>
                                    <option>Novo Cravinhos</option>
                                    <option>Novo Horizonte</option>
                                    <option>Nuporanga</option>
                                    <option>Oasis</option>
                                    <option>Ocaucu</option>
                                    <option>Oleo</option>
                                    <option>Olimpia</option>
                                    <option>Oliveira Barros</option>
                                    <option>Onda Branca</option>
                                    <option>Onda Verde</option>
                                    <option>Oriente</option>
                                    <option>Orindiuva</option>
                                    <option>Orlandia</option>
                                    <option>Osasco</option>
                                    <option>Oscar Bressane</option>
                                    <option>Osvaldo Cruz</option>
                                    <option>Ourinhos</option>
                                    <option>Ouro Fino Paulista</option>
                                    <option>Ouro Verde</option>
                                    <option>Ouroeste</option>
                                    <option>Pacaembu</option>
                                    <option>Padre Nobrega</option>
                                    <option>Palestina</option>
                                    <option>Palmares Paulista</option>
                                    <option>Palmeira D'oeste</option>
                                    <option>Palmeiras de Sao Paulo</option>
                                    <option>Palmital</option>
                                    <option>Panorama</option>
                                    <option>Paraguacu Paulista</option>
                                    <option>Paraibuna</option>
                                    <option>Paraiso</option>
                                    <option>Paraisolandia</option>
                                    <option>Paranabi</option>
                                    <option>Paranapanema</option>
                                    <option>Paranapiacaba</option>
                                    <option>Paranapua</option>
                                    <option>Parapua</option>
                                    <option>Pardinho</option>
                                    <option>Pariquera-acu</option>
                                    <option>Parisi</option>
                                    <option>Parnaso</option>
                                    <option>Parque Meia Lua</option>
                                    <option>Paruru</option>
                                    <option>Patrocinio Paulista</option>
                                    <option>Pauliceia</option>
                                    <option>Paulinia</option>
                                    <option>Paulistania</option>
                                    <option>Paulo de Faria</option>
                                    <option>Paulopolis</option>
                                    <option>Pederneiras</option>
                                    <option>Pedra Bela</option>
                                    <option>Pedra Branca de Itarare</option>
                                    <option>Pedranopolis</option>
                                    <option>Pedregulho</option>
                                    <option>Pedreira</option>
                                    <option>Pedrinhas Paulista</option>
                                    <option>Pedro Barros</option>
                                    <option>Pedro de Toledo</option>
                                    <option>Penapolis</option>
                                    <option>Pereira Barreto</option>
                                    <option>Pereiras</option>
                                    <option>Peruibe</option>
                                    <option>Piacatu</option>
                                    <option>Picinguaba</option>
                                    <option>Piedade</option>
                                    <option>Pilar do Sul</option>
                                    <option>Pindamonhangaba</option>
                                    <option>Pindorama</option>
                                    <option>Pinhalzinho</option>
                                    <option>Pinheiros</option>
                                    <option>Pioneiros</option>
                                    <option>Piquerobi</option>
                                    <option>Piquete</option>
                                    <option>Piracaia</option>
                                    <option>Piracicaba</option>
                                    <option>Piraju</option>
                                    <option>Pirajui</option>
                                    <option>Piramboia</option>
                                    <option>Pirangi</option>
                                    <option>Pirapitingui</option>
                                    <option>Pirapora do Bom Jesus</option>
                                    <option>Pirapozinho</option>
                                    <option>Pirassununga</option>
                                    <option>Piratininga</option>
                                    <option>Pitangueiras</option>
                                    <option>Planalto</option>
                                    <option>Planalto do Sul</option>
                                    <option>Platina</option>
                                    <option>Poa</option>
                                    <option>Poloni</option>
                                    <option>Polvilho</option>
                                    <option>Pompeia</option>
                                    <option>Pongai</option>
                                    <option>Pontal</option>
                                    <option>Pontalinda</option>
                                    <option>Pontes Gestal</option>
                                    <option>Populina</option>
                                    <option>Porangaba</option>
                                    <option>Porto Feliz</option>
                                    <option>Porto Ferreira</option>
                                    <option>Porto Novo</option>
                                    <option>Potim</option>
                                    <option>Potirendaba</option>
                                    <option>Potunduva</option>
                                    <option>Pracinha</option>
                                    <option>Pradinia</option>
                                    <option>Pradopolis</option>
                                    <option>Praia Grande</option>
                                    <option>Pratania</option>
                                    <option>Presidente Alves</option>
                                    <option>Presidente Bernardes</option>
                                    <option>Presidente Epitacio</option>
                                    <option>Presidente Prudente</option>
                                    <option>Presidente Venceslau</option>
                                    <option>Primavera</option>
                                    <option>Promissao</option>
                                    <option>Prudencio E Moraes</option>
                                    <option>Quadra</option>
                                    <option>Quata</option>
                                    <option>Queiroz</option>
                                    <option>Queluz</option>
                                    <option>Quintana</option>
                                    <option>Quiririm</option>
                                    <option>Rafard</option>
                                    <option>Rancharia</option>
                                    <option>Rechan</option>
                                    <option>Redencao da Serra</option>
                                    <option>Regente Feijo</option>
                                    <option>Reginopolis</option>
                                    <option>Registro</option>
                                    <option>Restinga</option>
                                    <option>Riacho Grande</option>
                                    <option>Ribeira</option>
                                    <option>Ribeirao Bonito</option>
                                    <option>Ribeirao Branco</option>
                                    <option>Ribeirao Corrente</option>
                                    <option>Ribeirao do Sul</option>
                                    <option>Ribeirao dos Indios</option>
                                    <option>Ribeirao Grande</option>
                                    <option>Ribeirao Pires</option>
                                    <option>Ribeirao Preto</option>
                                    <option>Ribeiro do Vale</option>
                                    <option>Ribeiro dos Santos</option>
                                    <option>Rifaina</option>
                                    <option>Rincao</option>
                                    <option>Rinopolis</option>
                                    <option>Rio Claro</option>
                                    <option>Rio das Pedras</option>
                                    <option>Rio Grande da Serra</option>
                                    <option>Riolandia</option>
                                    <option>Riversul</option>
                                    <option>Roberto</option>
                                    <option>Rosalia</option>
                                    <option>Rosana</option>
                                    <option>Roseira</option>
                                    <option>Rubiacea</option>
                                    <option>Rubiao Junior</option>
                                    <option>Rubineia</option>
                                    <option>Ruilandia</option>
                                    <option>Sabauna</option>
                                    <option>Sabino</option>
                                    <option>Sagres</option>
                                    <option>Sales</option>
                                    <option>Sales Oliveira</option>
                                    <option>Salesopolis</option>
                                    <option>Salmourao</option>
                                    <option>Saltinho</option>
                                    <option>Salto</option>
                                    <option>Salto de Pirapora</option>
                                    <option>Salto do Avanhandava</option>
                                    <option>Salto Grande</option>
                                    <option>Sandovalina</option>
                                    <option>Santa Adelia</option>
                                    <option>Santa Albertina</option>
                                    <option>Santa America</option>
                                    <option>Santa Barbara D'oeste</option>
                                    <option>Santa Branca</option>
                                    <option>Santa Clara D'oeste</option>
                                    <option>Santa Cruz da Conceicao</option>
                                    <option>Santa Cruz da Esperanca</option>
                                    <option>Santa Cruz da Estrela</option>
                                    <option>Santa Cruz das Palmeiras</option>
                                    <option>Santa Cruz do Rio Pardo</option>
                                    <option>Santa Cruz dos Lopes</option>
                                    <option>Santa Ernestina</option>
                                    <option>Santa Eudoxia</option>
                                    <option>Santa Fe do Sul</option>
                                    <option>Santa Gertrudes</option>
                                    <option>Santa Isabel</option>
                                    <option>Santa Isabel do Marinheiro</option>
                                    <option>Santa Lucia</option>
                                    <option>Santa Margarida Paulista</option>
                                    <option>Santa Maria da Serra</option>
                                    <option>Santa Maria do Gurupa</option>
                                    <option>Santa Mercedes</option>
                                    <option>Santa Rita D'oeste</option>
                                    <option>Santa Rita do Passa Quatro</option>
                                    <option>Santa Rita do Ribeira</option>
                                    <option>Santa Rosa de Viterbo</option>
                                    <option>Santa Salete</option>
                                    <option>Santa Teresinha de Piracicaba</option>
                                    <option>Santana da Ponte Pensa</option>
                                    <option>Santana de Parnaiba</option>
                                    <option>Santelmo</option>
                                    <option>Santo Anastacio</option>
                                    <option>Santo Andre</option>
                                    <option>Santo Antonio da Alegria</option>
                                    <option>Santo Antonio da Estiva</option>
                                    <option>Santo Antonio de Posse</option>
                                    <option>Santo Antonio do Aracangua</option>
                                    <option>Santo Antonio do Jardim</option>
                                    <option>Santo Antonio do Paranapanema</option>
                                    <option>Santo Antonio do Pinhal</option>
                                    <option>Santo Antonio Paulista</option>
                                    <option>Santo Expedito</option>
                                    <option>Santopolis do Aguapei</option>
                                    <option>Santos</option>
                                    <option>Sao Benedito da Cachoeirinha</option>
                                    <option>Sao Benedito das Areias</option>
                                    <option>Sao Bento do Sapucai</option>
                                    <option>Sao Bernardo do Campo</option>
                                    <option>Sao Berto</option>
                                    <option>Sao Caetano do Sul</option>
                                    <option>Sao Carlos</option>
                                    <option>Sao Francisco</option>
                                    <option>Sao Francisco da Praia</option>
                                    <option>Sao Francisco Xavier</option>
                                    <option>Sao Joao da Boa Vista</option>
                                    <option>Sao Joao das Duas Pontes</option>
                                    <option>Sao Joao de Iracema</option>
                                    <option>Sao Joao de Itaguacu</option>
                                    <option>Sao Joao do Marinheiro</option>
                                    <option>Sao Joao Do Pau D'alho</option>
                                    <option>Sao Joao Novo</option>
                                    <option>Sao Joaquim da Barra</option>
                                    <option>Sao Jose da Bela Vista</option>
                                    <option>Sao Jose das Laranjeiras</option>
                                    <option>Sao Jose do Barreiro</option>
                                    <option>Sao Jose do Rio Pardo</option>
                                    <option>Sao Jose do Rio Preto</option>
                                    <option>Sao Jose dos Campos</option>
                                    <option>Sao Lourenco da Serra</option>
                                    <option>Sao Lourenco do Turvo</option>
                                    <option>Sao Luis do Paraitinga</option>
                                    <option>Sao Luiz do Guaricanga</option>
                                    <option>Sao Manuel</option>
                                    <option>Sao Martinho D'oeste</option>
                                    <option>Sao Miguel Arcanjo</option>
                                    <option>Sao Paulo</option>
                                    <option>Sao Pedro</option>
                                    <option>Sao Pedro do Turvo</option>
                                    <option>Sao Roque</option>
                                    <option>Sao Roque da Fartura</option>
                                    <option>Sao Sebastiao</option>
                                    <option>Sao Sebastiao da Grama</option>
                                    <option>Sao Sebastiao da Serra</option>
                                    <option>Sao Silvestre de Jacarei</option>
                                    <option>Sao Simao</option>
                                    <option>Sao Vicente</option>
                                    <option>Sapezal</option>
                                    <option>Sarapui</option>
                                    <option>Sarutaia</option>
                                    <option>Sebastianopolis do Sul</option>
                                    <option>Serra Azul</option>
                                    <option>Serra Negra</option>
                                    <option>Serrana</option>
                                    <option>Sertaozinho</option>
                                    <option>Sete Barras</option>
                                    <option>Severinia</option>
                                    <option>Silvania</option>
                                    <option>Silveiras</option>
                                    <option>Simoes</option>
                                    <option>Simonsen</option>
                                    <option>Socorro</option>
                                    <option>Sodrelia</option>
                                    <option>Solemar</option>
                                    <option>Sorocaba</option>
                                    <option>Sousas</option>
                                    <option>Sud Mennucci</option>
                                    <option>Suinana</option>
                                    <option>Sumare</option>
                                    <option>Sussui</option>
                                    <option>Suzanapolis</option>
                                    <option>Suzano</option>
                                    <option>Tabajara</option>
                                    <option>Tabapua</option>
                                    <option>Tabatinga</option>
                                    <option>Taboao da Serra</option>
                                    <option>Taciba</option>
                                    <option>Taguai</option>
                                    <option>Taiacu</option>
                                    <option>Taiacupeba</option>
                                    <option>Taiuva</option>
                                    <option>Talhado</option>
                                    <option>Tambau</option>
                                    <option>Tanabi</option>
                                    <option>Tapinas</option>
                                    <option>Tapirai</option>
                                    <option>Tapiratiba</option>
                                    <option>Taquaral</option>
                                    <option>Taquaritinga</option>
                                    <option>Taquarituba</option>
                                    <option>Taquarivai</option>
                                    <option>Tarabai</option>
                                    <option>Taruma</option>
                                    <option>Tatui</option>
                                    <option>Taubate</option>
                                    <option>Tecainda</option>
                                    <option>Tejupa</option>
                                    <option>Teodoro Sampaio</option>
                                    <option>Termas de Ibira</option>
                                    <option>Terra Nova D'oeste</option>
                                    <option>Terra Roxa</option>
                                    <option>Tibirica</option>
                                    <option>Tibirica do Paranapanema</option>
                                    <option>Tiete</option>
                                    <option>Timburi</option>
                                    <option>Toledo</option>
                                    <option>Torre de Pedra</option>
                                    <option>Torrinha</option>
                                    <option>Trabiju</option>
                                    <option>Tremembe</option>
                                    <option>Tres Aliancas</option>
                                    <option>Tres Fronteiras</option>
                                    <option>Tres Pontes</option>
                                    <option>Tres Vendas</option>
                                    <option>Tuiuti</option>
                                    <option>Tujuguaba</option>
                                    <option>Tupa</option>
                                    <option>Tupi</option>
                                    <option>Tupi Paulista</option>
                                    <option>Turiba do Sul</option>
                                    <option>Turiuba</option>
                                    <option>Turmalina</option>
                                    <option>Turvinia</option>
                                    <option>Ubarana</option>
                                    <option>Ubatuba</option>
                                    <option>Ubirajara</option>
                                    <option>Uchoa</option>
                                    <option>Uniao Paulista</option>
                                    <option>Universo</option>
                                    <option>Urania</option>
                                    <option>Uru</option>
                                    <option>Urupes</option>
                                    <option>Ururai</option>
                                    <option>Utinga</option>
                                    <option>Vale Formoso</option>
                                    <option>Valentim Gentil</option>
                                    <option>Valinhos</option>
                                    <option>Valparaiso</option>
                                    <option>Vangloria</option>
                                    <option>Vargem</option>
                                    <option>Vargem Grande do Sul</option>
                                    <option>Vargem Grande Paulista</option>
                                    <option>Varpa</option>
                                    <option>Varzea Paulista</option>
                                    <option>Venda Branca</option>
                                    <option>Vera Cruz</option>
                                    <option>Vicente de Carvalho</option>
                                    <option>Vicentinopolis</option>
                                    <option>Vila Dirce</option>
                                    <option>Vila Nery</option>
                                    <option>Vila Xavier</option>
                                    <option >Vinhedo</option>
                                    <option>Viradouro</option>
                                    <option>Vista Alegre do Alto</option>
                                    <option>Vitoria Brasil</option>
                                    <option>Vitoriana</option>
                                    <option>Votorantim</option>
                                    <option>Votuporanga</option>
                                    <option>Zacarias</option>
                                </optgroup>
                                </select></p>
                                <p><label for="email"><b>E-mail*:</b></label>
                                    <input type="text" maxlength="50" name="cliente_email" value="<?php echo "$email" ?>" placeholder="Digite seu e-mail"/>
                                <p><b>Tel. Res.*:</b> <select name="cliente_ddd_telres" id="cliente_ddd_telres" class="sel_ddd">
                                <optgroup label="DDD selecionado pelo usu&aacute;rio">
                                    <option><?php echo "$dddtelres" ?></option>
                                </optgroup>
                                <optgroup label="DDD's dispon&iacute;veis">
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                    <option>16</option>
                                    <option>17</option>
                                    <option>18</option>
                                    <option>19</option>
                                    <option>21</option>
                                    <option>22</option>
                                    <option>24</option>
                                    <option>27</option>
                                    <option>28</option>
                                    <option>31</option>
                                    <option>32</option>
                                    <option>33</option>
                                    <option>34</option>
                                    <option>35</option>
                                    <option>37</option>
                                    <option>38</option>
                                    <option>41</option>
                                    <option>42</option>
                                    <option>42</option>
                                    <option>43</option>
                                    <option>44</option>
                                    <option>45</option>
                                    <option>46</option>
                                    <option>47</option>
                                    <option>48</option>
                                    <option>49</option>
                                    <option>51</option>
                                    <option>53</option>
                                    <option>54</option>
                                    <option>55</option>
                                    <option>61</option>
                                    <option>61</option>
                                    <option>62</option>
                                    <option>63</option>
                                    <option>64</option>
                                    <option>65</option>
                                    <option>66</option>
                                    <option>67</option>
                                    <option>68</option>
                                    <option>69</option>
                                    <option>71</option>
                                    <option>73</option>
                                    <option>74</option>
                                    <option>75</option>
                                    <option>77</option>
                                    <option>79</option>
                                    <option>81</option>
                                    <option>82</option>
                                    <option>83</option>
                                    <option>84</option>
                                    <option>85</option>
                                    <option>86</option>
                                    <option>87</option>
                                    <option>88</option>
                                    <option>89</option>
                                    <option>91</option>
                                    <option>92</option>
                                    <option>93</option>
                                    <option>94</option>
                                    <option>95</option>
                                    <option>96</option>
                                    <option>97</option>
                                    <option>98</option>
                                    <option>99</option>
                                </optgroup>
                                </select> 
                                <input type="text" name="cliente_telres" value="<?php echo "$telres" ?>" maxlength="20" placeholder="Digite seu telefone residencial"/>        
                                Tel. Com.: <select name="cliente_ddd_telcom" id="cliente_ddd_telcom" class="sel_ddd">
                                <optgroup label="DDD selecionado pelo usu&aacute;rio">
                                    <option><?php echo "$dddtelcom" ?></option>
                                </optgroup>
                                <optgroup label="DDD's dispon&iacute;veis">
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                    <option>16</option>
                                    <option>17</option>
                                    <option>18</option>
                                    <option>19</option>
                                    <option>21</option>
                                    <option>22</option>
                                    <option>24</option>
                                    <option>27</option>
                                    <option>28</option>
                                    <option>31</option>
                                    <option>32</option>
                                    <option>33</option>
                                    <option>34</option>
                                    <option>35</option>
                                    <option>37</option>
                                    <option>38</option>
                                    <option>41</option>
                                    <option>42</option>
                                    <option>42</option>
                                    <option>43</option>
                                    <option>44</option>
                                    <option>45</option>
                                    <option>46</option>
                                    <option>47</option>
                                    <option>48</option>
                                    <option>49</option>
                                    <option>51</option>
                                    <option>53</option>
                                    <option>54</option>
                                    <option>55</option>
                                    <option>61</option>
                                    <option>61</option>
                                    <option>62</option>
                                    <option>63</option>
                                    <option>64</option>
                                    <option>65</option>
                                    <option>66</option>
                                    <option>67</option>
                                    <option>68</option>
                                    <option>69</option>
                                    <option>71</option>
                                    <option>73</option>
                                    <option>74</option>
                                    <option>75</option>
                                    <option>77</option>
                                    <option>79</option>
                                    <option>81</option>
                                    <option>82</option>
                                    <option>83</option>
                                    <option>84</option>
                                    <option>85</option>
                                    <option>86</option>
                                    <option>87</option>
                                    <option>88</option>
                                    <option>89</option>
                                    <option>91</option>
                                    <option>92</option>
                                    <option>93</option>
                                    <option>94</option>
                                    <option>95</option>
                                    <option>96</option>
                                    <option>97</option>
                                    <option>98</option>
                                    <option>99</option>
                                </optgroup>
                                </select>
                                <input type="text" maxlength="20" name="cliente_telcom" value="<?php echo "$telcom" ?>" placeholder="Digite seu telefone comercial"  /><br />          
                                <b>Tel. Cel.*:</b> <select name="cliente_ddd_telcel" id="cliente_ddd_telcel" class="sel_ddd">
                                <optgroup label="DDD selecionado pelo usu&aacute;rio">
                                    <option><?php echo "$dddtelcel" ?></option>
                                </optgroup>
                                <optgroup label="DDD's dispon&iacute;veis">
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                    <option>16</option>
                                    <option>17</option>
                                    <option>18</option>
                                    <option>19</option>
                                    <option>21</option>
                                    <option>22</option>
                                    <option>24</option>
                                    <option>27</option>
                                    <option>28</option>
                                    <option>31</option>
                                    <option>32</option>
                                    <option>33</option>
                                    <option>34</option>
                                    <option>35</option>
                                    <option>37</option>
                                    <option>38</option>
                                    <option>41</option>
                                    <option>42</option>
                                    <option>42</option>
                                    <option>43</option>
                                    <option>44</option>
                                    <option>45</option>
                                    <option>46</option>
                                    <option>47</option>
                                    <option>48</option>
                                    <option>49</option>
                                    <option>51</option>
                                    <option>53</option>
                                    <option>54</option>
                                    <option>55</option>
                                    <option>61</option>
                                    <option>61</option>
                                    <option>62</option>
                                    <option>63</option>
                                    <option>64</option>
                                    <option>65</option>
                                    <option>66</option>
                                    <option>67</option>
                                    <option>68</option>
                                    <option>69</option>
                                    <option>71</option>
                                    <option>73</option>
                                    <option>74</option>
                                    <option>75</option>
                                    <option>77</option>
                                    <option>79</option>
                                    <option>81</option>
                                    <option>82</option>
                                    <option>83</option>
                                    <option>84</option>
                                    <option>85</option>
                                    <option>86</option>
                                    <option>87</option>
                                    <option>88</option>
                                    <option>89</option>
                                    <option>91</option>
                                    <option>92</option>
                                    <option>93</option>
                                    <option>94</option>
                                    <option>95</option>
                                    <option>96</option>
                                    <option>97</option>
                                    <option>98</option>
                                    <option>99</option>
                                </optgroup>
                                </select>
                                
                                <input type="text" maxlength="20" name="cliente_telcel" value="<?php echo "$telcel" ?>" placeholder="Digite seu telefone celular" /></p>
                        </fieldset>
                        <br />
                        <fieldset>
                            <legend><h1>Faixa et&aacute;ria</h1></legend> **selecionar pelo menos 1 campo



                            <p>De 00 at&eacute; 18: <input type="text" maxlength="1" style="width:22px;" name="idade_1" value="<?php echo "$idade_0018" ?>" id="idade_0018" />
                            De 19 at&eacute; 23: <input type="text" maxlength="1" style="width:22px;" name="idade_2" value="<?php echo "$idade_1923" ?>" id="idade_1923" />
                            De 24 at&eacute; 28: <input type="text" maxlength="1" style="width:22px;" name="idade_3" value="<?php echo "$idade_2428" ?>" id="idade_2428" />
                            De 29 at&eacute; 33: <input type="text" maxlength="1" style="width:22px;" name="idade_4" value="<?php echo "$idade_2933" ?>" id="idade_2933" />      
                            De 34 at&eacute; 38: <input type="text" maxlength="1" style="width:22px;" name="idade_5" value="<?php echo "$idade_3438" ?>" id="idade_3438" /></p>

                            <p>De 39 at&eacute; 43: <input type="text" maxlength="1" style="width:22px;" name="idade_6" value="<?php echo "$idade_3943" ?>" id="idade_3943" />    
                            De 44 at&eacute; 48: <input type="text" maxlength="1" style="width:22px;" name="idade_7" value="<?php echo "$idade_4448" ?>" id="idade_4448" />    
                            De 49 at&eacute; 53: <input type="text" maxlength="1" style="width:22px;" name="idade_8" value="<?php echo "$idade_4953" ?>" id="idade_4953" />     
                            De 54 at&eacute; 58: <input type="text" maxlength="1" style="width:22px;" name="idade_9" value="<?php echo "$idade_5458" ?>" id="idade_5458" />      
                            De 59 para cima: <input type="text" maxlength="1" style="width:22px;" name="idade_10" value="<?php echo "$maior59" ?>" id="idade_maior_igual59" /></p>
                            </fieldset>
                        <br />

                        <fieldset>

                            <legend><h1>Informa&ccedil;&otilde;es adicionais</h1></legend>

                            <p>Informa&ccedil;&otilde;es adicionais:</p>
                            <textarea maxlength="255" name="cliente_info" rows="10" cols="40"><?php echo "$info" ?></textarea>
                                
                                <input type="hidden" value="<?=$boletim ?>" name="cliente_boletim">

                            <p>Gostaria de receber nosso boletim informativo: <br />
                            <input type="radio" name="cliente_boletim" value="Sim" <?php if ($boletim == "Sim") {
                                echo "Checked";
                            } ?> /> Sim
                            <input type="radio" name="cliente_boletim" value="Nao" <?php if ($boletim == "Nao") {
                                echo "Checked";
                            } ?>/> N&atilde;o
                            &nbsp;<br />
                            </fieldset>
                        <br />
                        <input type="hidden" name="usuario_id" id ="usuario_id" value ="<?=$_GET["id"]?>" />
                        <br /><br />
                        <br /><input type="submit" value="Atualizar" />
                        <input type="reset" value="Limpar Dados"> 
                    </form>
            </div>
        </div>
            <?php }?>
        </div>
        <div id="wrapper"></div>
        <?php require_once ("includes/footer.php"); ?>
    </body>
</html>